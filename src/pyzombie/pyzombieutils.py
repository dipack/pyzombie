import requests
import random
import time
import logging
import loremipsum as loi
import validators
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup as bs

from enums.categories import Categories
from enums.websites import Websites

logger = logging.getLogger(__name__)


class PyZombieUtils:
    LoiGen = None

    def __init__(self):
        logging.basicConfig(format='[%(asctime)s] [%(levelname)s] %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                            level=logging.INFO)
        with open('../../resources/words/words2.txt') as dictionary_text:
            dictionary = dictionary_text.read().split()
        self.LoiGen = loi.Generator(sample="The quick brown fox jumps over the lazy dog", dictionary=dictionary)

    @staticmethod
    def check_internet_connection():
        response = requests.get("http://www.google.com")
        return response.status_code == 200

    @staticmethod
    def check_if_url_is_relative(url):
        return bool(urlparse(url).netloc)

    def get_all_links(self, url):
        if not self.check_internet_connection():
            logger.error("No internet connection!")
            return []
        response = requests.get(url)
        links = bs(response.content).find_all('a')
        actual_links = []
        for link in links:
            href = link.get('href')
            if not self.check_if_url_is_relative(url=href):
                href = urljoin(url, href)
            if validators.url(href):
                actual_links.append(href)
        return actual_links

    def visit_links_on_site(self, url, num_links_to_visit=10, time_to_linger=1):
        if not self.check_internet_connection():
            logger.error("No internet connection!")
            return
        actual_links = self.get_all_links(url=url)
        try:
            for _ in range(num_links_to_visit):
                choice = random.choice(actual_links)
                logger.info("Visiting: %s" % choice)
                response = requests.get(choice)
                if response.status_code == 200:
                    logger.debug("Successfully visited %s" % choice)
                else:
                    logger.warning("Could not hit %s" % choice)
                if not self.roll_dice_to_continue():
                    logger.info("Aborting the rest of our journey on these seas!")
                    return
                time.sleep(time_to_linger)
        except IndexError:
            logger.error("No links found at %s" % url)
            return
        return

    def send_email(self, user, pwd, recipient, subject, body):
        import smtplib

        gmail_user = user
        gmail_pwd = pwd
        FROM = user
        TO = recipient if type(recipient) is list else [recipient]
        SUBJECT = subject
        TEXT = body

        # Prepare actual message
        message = """From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, message)
            server.close()
            logger.info('Successfully sent the mail')
        except Exception:
            logger.error("Failed to send mail")
        return

    def random_action_generator(self, seed=100):
        actions = [self.send_email, self.visit_links_on_site]
        random.seed(seed)
        choice = random.choice(actions)
        logger.info("Running %s" % choice)
        choice()
        return

    def random_sentence_generator(self, amount=1, generate_paragraphs=False):
        if not generate_paragraphs:
            generated = self.LoiGen.generate_sentences(amount=amount)
        else:
            generated = self.LoiGen.generate_paragraphs(amount=amount)
        generated_data = generated.__next__()[2]
        logger.info("Generated %s" % generated_data)
        return

    def roll_dice_to_continue(self, numberOfDice=10):
        shall_we_continue = (abs(random.randrange(numberOfDice) - random.randrange(numberOfDice)) % 2)
        logger.info("Rolled a %s" % shall_we_continue)
        return shall_we_continue

    def real_action(self):
        site = random.choice(Websites.websites)
        logger.info("Where are we going next? To %s" % site)
        if Categories.Mail in site['categories']:
            logger.info("Will send email!")
            # self.send_email(pc.PersonalConfig.Email, pc.PersonalConfig.Password, pc.PersonalConfig.ToEmail, "Test subject",
            #                 "Test body")
        else:
            self.visit_links_on_site(url=site['url'], num_links_to_visit=random.randrange(10))


if __name__ == '__main__':
    pyz = PyZombieUtils()
    while 1:
        pyz.real_action()
