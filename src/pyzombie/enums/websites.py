from .categories import Categories


class Websites:
    websites = [
        {
            'name': 'New York Times',
            'url': 'https://nytimes.com',
            'categories': [Categories.News, Categories.Education]
        },
        {
            'name': 'Gmail',
            'url': 'https://gmail.com',
            'categories': [Categories.Mail]
        },
        {
            'name': 'Yahoo! Answers',
            'url': 'https://answers.yahoo.com/',
            'categories': [Categories.Education, Categories.MessageBoard]
        },
        {
            'name': 'Reddit',
            'url': 'https://www.reddit.com/',
            'categories': [Categories.MessageBoard, Categories.News]
        },
        {
            'name': 'YouTube',
            'url': 'https://www.youtube.com/',
            'categories': [Categories.Entertainment]
        },
        {
            'name': 'Rakuten',
            'url': 'https://global.rakuten.com/corp/worldwide/',
            'categories': [Categories.Corporate]
        },
        {
            'name': 'Zhihu',
            'url': 'https://www.zhihu.com/',
            'categories': [Categories.MessageBoard]
        },
        {
            'name': 'FC2',
            'url': 'https://fc2.com/',
            'categories': [Categories.Entertainment]
        }
    ]
