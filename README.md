# pyzombie
Zombie script written in Python to impersonate a normal user.

## Why is this a thing?
It's quite useful in hopefully obscuring your browsing history, by randomly browsing sites. I just made it as a proof of concept.
